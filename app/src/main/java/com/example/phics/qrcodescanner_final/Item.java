package com.example.phics.qrcodescanner_final;

/**
 * Created by jtbum on 4/4/2018.
 */

import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import android.content.Context;
import android.location.Address;
import java.io.Serializable;

public class Item implements Serializable  // test item is <70><30><3><Guitar><good condition, very expensive><true>
{
    private List<String> Locations;
    private String name;
    private String id;
    private String state;

    public Item()
    {

    }

    public Item(String title, String qr, List<String> l, String s)
    {
        name = title;
        Locations = l;
        id = qr;
        state = s;
    }

    public Item(int condition, String Item,boolean g,String ID)
    {
        name = Item;
    }

    public List<String> getLocations()
    {
        return Locations;
    }

    public String getCurrentLocation()
    {
        return Locations.get(Locations.size() - 1);
    }

    public void addLocation(String s)
    {
        Locations.add(s);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String n)
    {
        name = n;
    }

    public String getID()
    {
        return id;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String s)
    {
        state = s;
    }

    public String toString()
    {
        return name;
    }
}
