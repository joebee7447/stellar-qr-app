package com.example.phics.qrcodescanner_final;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.view.View;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.view.View;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import android.widget.ArrayAdapter;
import android.app.ListActivity;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Inventory extends AppCompatActivity {

    ListView listview;
    ArrayList<Item> itemList = new ArrayList<>();    //used for a later date for items
    String inventoryString;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_table);
        final Activity activity = this;
        getItem test = new getItem();
        test.start();
        //infinite loop to allow iList to receive value from request
        while(test.iList == null){

        }
        //JSON setup
        try {
            JSONArray jsonArray = new JSONArray(test.iList);
            for(int i = 0; i < jsonArray.length(); ++i){
                JSONObject c = jsonArray.getJSONObject(i);
                String ID = c.getString("ID");
                System.out.println(ID);
                String Item = c.getString("Item");
                String UserID = c.getString("UserID");
                Item tryToAdd = new Item(1, Item, true, ID);
                itemList.add(tryToAdd);

            }
        }
        catch(final JSONException e){
            System.out.println("not working");
        }
        test.iList = null;

        listview = (ListView)findViewById(R.id.listLayout);
        /*itemList.add(new Item(1, "Guitar", true, "1"));
        itemList.add(new Item(1, "Drums", true, "2"));
        itemList.add(new Item(1, "Bass", true, "3"));
        itemList.add(new Item(1, "Cello", true, "4"));
        itemList.add(new Item(1, "Trombone", true, "5"));
        itemList.add(new Item(1, "Trumpet", true, "6"));
        itemList.add(new Item(1, "Banjo", true, "7"));
*/
        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, itemList);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                final Item i = (Item) parent.getItemAtPosition(position);
                Intent intent = new Intent(activity, InventoryDetails.class);
                String message = "Item ID: " + i.getID() + "\nItem Name: " + i.getName() + "\nCurrent Location: ";
                intent.putExtra("key", message);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent i = new Intent(Inventory.this, Scanner.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }


}
