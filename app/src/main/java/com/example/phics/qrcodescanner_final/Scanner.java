package com.example.phics.qrcodescanner_final;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.view.View;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.*;
import java.sql.DriverManager;

import android.widget.EditText;
import com.google.zxing.Result;
import android.location.Geocoder;
import android.os.Looper;
import java.io.IOException;
import java.sql.*;
import android.content.Context;
import android.location.Address;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;


public class Scanner extends AppCompatActivity{

    private LocationManager locationManager;
    private LocationListener listener;
    java.util.Date currentTime;
    //////////////////////////////////////////////////////////////////////
    private static final int REQUEST_LOCATION = 10;     //variables
    private boolean scannerOpen = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);     //gets location service

//////////////////////////////////////////////////////////////////////

        configureButton();

//////////////////////////////////////////////////////////////////////
        final Activity activity = this;
    }

   public void openScanner(View v)
   {
       Intent intent = new Intent(this, OpenScanner.class);
       startActivity(intent);
   }



    public void configureButton() {                         //checks permission to access the users location
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
    }





    public void openMainMenu(View v)
    {
        setContentView(R.layout.getting_location);

        listener = new LocationListener() { //added location listener
            @Override
            public void onLocationChanged(Location location) {  //outputs a location update when the scanner is opened
                Geocoder geocoder;
                List<Address> a = null;
                geocoder = new Geocoder(Scanner.this, Locale.getDefault());

                try {
                    a = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                currentTime = Calendar.getInstance().getTime();

                String loc = a.get(0).getAddressLine(0);
                setContentView(R.layout.main_menu);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        Looper l = null;
        locationManager.requestSingleUpdate("gps", listener, l);

    }

    public void openInventory(View view)
    {

        Intent intent = new Intent(this, Inventory.class);
        startActivity(intent);
    }
}
